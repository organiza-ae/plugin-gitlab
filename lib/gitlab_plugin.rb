class GitlabPlugin < Noosfero::Plugin

  # https://github.com/gitlabhq/gitlabhq/tree/master/doc/api

  def initialize(context=nil)
    @@current_instance = self
    super context
  end

  def self.plugin_name
    _("GitLab Integration")
  end

  def self.plugin_description
    _("Display GitLab information on this social network.")
  end

  def stylesheet?
    true
  end

  def settings
    @settings ||= Noosfero::Plugin::Settings.new(
      context.environment, GitlabPlugin
    )
  end

  def self.extra_blocks
    {
      GitlabPlugin::GroupBlock => {},
      GitlabPlugin::ProjectBlock => {}
    }
  end

  def control_panel_buttons
    p = context.profile.identifier
    {
      title: _('GitLab Integration'),
      icon: 'gitlab',
      url: { controller: 'gitlab_plugin_myprofile', action: 'configure', profile: p }
    }
  end

  def self.settings(source, data=nil)
    Noosfero::Plugin::Settings.new source, GitlabPlugin, data
  end

  def self.get_setting(key)
    ctx = @@current_instance.context
    if ctx.kind_of? ApplicationController
      profile = ctx.profile
      env = ctx.environment
    elsif ctx.kind_of? Profile
      profile = ctx
      env = ctx.environment
    elsif ctx.kind_of? Box
      profile = ctx.owner
      env = ctx.environment
    else
      throw "Unknown context (#{ctx.class.name})."
    end
    value = GitlabPlugin.settings(profile).send(key)
    value = GitlabPlugin.settings(env).send(key) if value.blank?
    value
  end

end
