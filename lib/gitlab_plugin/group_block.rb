class GitlabPlugin::GroupBlock < Block

  settings_items :group_id, :type => :integer
  attr_accessible :group_id

  def self.description
    _('GitLab Group')
  end

  def help
    _("Display a GitLab's group information.")
  end

  def default_title
    if group_data[:ok]
      group_data[:group]['name']
    else
      _('GitLab Group')
    end
  end

  # Cached method. Requires information from GitLab API.
  # Dev useful snippet: $ rails r Rails.cache.clear
  def group_data
    if group_id.blank? || group_id == 0
      return { error: { code: '1', message: _('No group defined.') }, ok: false }
    end
    domain = GitlabPlugin.get_setting(:domain) || 'gitlab.com'
    token = GitlabPlugin.get_setting(:private_token)
    cache_key = "#{domain}/group-#{group_id}/#{token}"
    Rails.cache.fetch cache_key, expires_in:12.hours do
      require 'net/http'
      uri = URI("https://#{domain}/api/v3/groups/#{group_id}?private_token=#{token}")
      connection = Net::HTTP.new(uri.host, uri.port)
      connection.use_ssl = true
      res = connection.request_get(uri.path + '?' + uri.query)
      if res.is_a?(Net::HTTPSuccess)
        { group: JSON.parse(res.body), ok: true }
      else
        { error: { code: res.code, message: res.message }, ok: false }
      end
    end
  end

  def content(args={})
    block = self
    data = block.group_data
    proc do
      block_title(block.title) +
      if data[:ok] && data[:group]['projects']
        content_tag(:ul) do
          data[:group]['projects'].map do |project|
            content_tag :li do
              link_to (
                tag(:img, src: project['avatar_url']) +
                content_tag(:h3, project['name']) +
                content_tag(:p, project['description'])
              ), project['web_url']
            end
          end.join "\n"
        end
      else
        if block.group_id.blank? || block.group_id == 0
          _('No group defined. Please, configure this block.')
        else
          content_tag :div, class: 'fail' do
            content_tag(:h3, _('API request fail')) +
            content_tag(:div, content_tag(:strong, _('HTTP Code:')) +' '+ data[:error][:code]) +
            content_tag(:div, content_tag(:strong, _('Message:')) +' '+ data[:error][:message])
          end
        end
      end
    end
  end

end
