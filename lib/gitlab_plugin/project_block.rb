class GitlabPlugin::ProjectBlock < Block

  settings_items :group_id, :type => :integer

  def self.description
    _('GitLab Project')
  end

  def help
    _("Display a GitLab's project information.")
  end

  def default_title
    if group_data
      group_data['name']
    else
      _('GitLab Project')
    end
  end

  def project_data
    nil
  end

  def content(args={})
    block = self
    data = block.project_data
    proc do
      if data
        _('There is no data')
      else
        "data... #{data.inspect}"
      end
    end
  end

end
