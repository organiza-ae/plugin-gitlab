class GitlabPluginAdminController < PluginsController

  def index
    @settings ||= GitlabPlugin.settings environment, params[:settings]
    if request.post?
      @settings.save!
      redirect_to controller: 'plugins', action: 'index'
    end
  end

end
