class GitlabPluginMyprofileController < PublicController

  needs_profile

  def configure
    @settings ||= GitlabPlugin.settings profile, params[:settings]
    @settings.save! if request.post?
    render action: 'configure'
  end

end
